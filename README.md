ビルド済mesos-1.0.0 for Raspberry Pi 3
====

※Raspbian Jessie のバージョンは8.0 (ClusterHAT導入時のバージョン）

Raspberry Pi3でApache Mesos 1.0.0をソースコードからビルドすると自機環境では12時間以上かかりました（スワップファイルを2GBに拡張した状態で）。
このプロジェクトはビルド済ですのですぐにインストールできます。
また、オリジナルのソースのままビルドするとmesos-masterを起動する際に

	Check failed: leveldb::BytewiseComparator()->Compare(one, two) < 0
	*** Check failure stack trace: ***
	    @ 0x76657838  google::LogMessage::Fail()

でエラーとなり起動できないので

	./src/log/leveldb.cpp

のソースコードを一部修正しています。修正方法は[こちら](https://github.com/lyda/mesos-on-arm/compare/comparator_fix "mesos-on-arm Comparing changes")を参考にしました。なお、オリジナルのソースファイルは

	./src/log/leveldb.cpp.old

としてそのまま残してありますので、適宜改良してください。
mesos-1.0.0のREADME.mdは./README.md.mesosとして配置してあります。


## 使い方

ビルド済みなのでインストールを行うだけです。

    cd $HOME
    git clone https://gitlab.com/softdemo.harddemo.raspberrypi/mesos-1.0.0-built
    mv mesos-1.0.0-built/mesos-1.0.0.tgz .
    tar zxf mesos-1.0.0.tgz
    rm mesos-1.0.0.tgz
    rm -fr mesos-1.0.0-built
    cd mesos-1.0.0/build
	sudo make install

必要に応じて以下も実行してください。

	sudo ldconfig

